<?php

namespace Drupal\multi_step;

use Exception;

class Core {

  const INDEX = 'multi_step';
  const JUMP = '__multi_step_jump__';
  const SAVE = '__multi_step_save__';

  static function get($type, $hook) {
    static $cache = [];

    if (isset($cache[$type][$hook])) {
      return $cache[$type][$hook];
    }

    $class = __NAMESPACE__ . '\\' . ucfirst($type);

    try {
      $object = new $class($hook);
    } catch (Exception $exception) {
      $exception = NULL;
      return NULL;
    }

    $cache[$type][$hook] = $object;
    return $object;
  }

  static function session(&$form_state, $index = NULL) {
    $hook = static::state($form_state, 'hook');
    $item = &$_SESSION[static::INDEX][$hook];
    return static::result($item, $index);
  }

  static function state(&$form_state, $index = NULL) {
    $item = &$form_state[static::INDEX];
    return static::result($item, $index);
  }

  static protected function result(&$item, $index = NULL) {
    if (FALSE == isset($item)) {
      return NULL;
    }

    if (FALSE == isset($index)) {
      return $item;
    }

    $data = (array)$item;

    return isset($data[$index]) ? $data[$index] : NULL;
  }

  static function info(&$form_state, $index = NULL) {
    $hook = static::state($form_state, 'hook');
    $item = \Drupal\multi_step\Setting::get($hook);
    return static::result($item, $index);
  }

  static function form($form, &$form_state, $hook, $step = '') {
    //    $form_state['no_cache'] = TRUE;
    $form_state[static::INDEX]['hook'] = $hook;

    if (static::check($form_state, $step)) {
      return static::call($form, $form_state, $hook, __FUNCTION__);
    } else {
      return [];
    }
  }

  static protected function check(&$form_state, $step = '') {
    $index = 'step';
    $state = &$form_state[static::INDEX];

    if (isset($state[$index]) && (empty($step) || (FALSE == empty($form_state['rebuild'])))) {
      $step = $state[$index];
    }

    $hook = isset($state['hook']) ? $state['hook'] : NULL;
    $info = \Drupal\multi_step\Setting::get($hook);

    if (isset($info) && (FALSE == $info->exist($step))) {
      $step = $info->firstStep();
    }

    if (empty($hook) || empty($step)) {
      form_set_error('', t('Can not load %name multi-step setting or invaild step.', ['%name' => $hook]));
      return FALSE;
    }

    $state[$index] = $step;
    return $step;
  }

  static protected function call($form, &$form_state, $hook, $method) {
    $proxy = \Drupal\multi_step\Proxy::get($hook);

    if (is_callable([
      $proxy,
      $method
    ])) {
      return $proxy->{$method}($form, $form_state);
    } else {
      return [];
    }
  }

  static function validate($form, &$form_state) {
    if (static::check($form_state)) {
      static::call($form, $form_state, $form_state[static::INDEX]['hook'], __FUNCTION__);
    }
  }

  static function submit($form, &$form_state) {
    if (static::check($form_state)) {
      static::call($form, $form_state, $form_state[static::INDEX]['hook'], __FUNCTION__);
    }
  }
}
