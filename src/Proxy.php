<?php
namespace Drupal\multi_step;

use Exception;

class Proxy {

  /** @var Setting */
  var $setting = NULL;
  var $step = '';

  function __construct($hook) {
    $this->setting = Setting::get($hook);

    if (FALSE == isset($this->setting)) {
      throw new Exception(t('Can not load multi-step setting.'));
    }
  }

  /** @return Proxy */
  static function get($hook) {
    return Core::get('proxy', $hook);
  }

  function __get($name) {
    return isset($this->$name) ? $this->$name : NULL;
  }

  function form($form, &$form_state) {
    $this->setStep($form_state);

    if (FALSE == empty($form_state['input']) && empty($form_state['values'])) {
      $this->prepareSumbit($form_state);
      drupal_form_submit('multi_step_form', $form_state);
    }

    $form = $this->formBuilder($form);
    $result = $this->call($form, $form_state, 'form');
    return (FALSE === $result) ? $form : $result;
  }

  protected function setStep(&$form_state) {
    $this->step = $form_state[Core::INDEX]['step'];
  }

  protected function prepareSumbit(&$form_state) {
    $session = &$_SESSION[Core::INDEX][$this->setting->hook];

    if (FALSE == isset($session)) {
      $session = [];
    }

    $this->prepareJump($form_state);
    unset($form_state['input'][Core::SAVE]);
    $form_state['values'] = $form_state['input'];
  }

  protected function formBuilder($form) {
    $step = $this->step;
    $info = $this->setting;
    $steps = $info->formSteps($step);

    if ($info->stepItem($step)->multiforms) {
      $form['steps'] = $steps;
      $form['__'] = ['#type' => 'form'];
    } else {
      $form['#type'] = 'form';
      $form['#action'] = $info->url($step);
      $form['actions'] = $info->formActions($step);
      $form['steps'] = $steps;
    }

    return $form;
  }

  protected function call($form, &$form_state, $type) {
    $call = $this->setting->hook . '_' . $this->step . '_' . $type;

    try {
      return is_callable($call) ? $call($form, $form_state) : FALSE;
    } catch (Exception $exception) {
      form_set_error('', $exception->getMessage());
      return FALSE;
    }
  }

  protected function prepareJump(&$form_state) {
    $index = Core::INDEX;
    $jump = Core::JUMP;
    $setting = $this->setting;
    $input = &$form_state['input'][Core::SAVE];
    $session = &$_SESSION[$index][$setting->hook];

    if ((FALSE == isset($input)) || (FALSE == is_array($input))) {
      return;
    }

    foreach ($input as $key => $item) {
      if ($setting->exist($key) && is_array($item)) {
        $session[$key] = $item;
      }
    }

    if (isset($input[$jump]) && $setting->exist($input[$jump])) {
      $form_state[$index]['jump'] = $input[$jump];
    }
  }

  function validate($form, &$form_state) {
    $this->setStep($form_state);

    if ($this->setting->stepItem($this->step)->multiforms) {
      $form_state['values'] = $form_state['input'];
    }

    $this->call($form, $form_state, 'form_validate');
  }

  function submit($form, &$form_state) {
    $this->setStep($form_state);
    $index = Core::INDEX;
    $setting = $this->setting;
    $hook = $setting->hook;
    $step = $this->step;
    $values = &$form_state['values'];
    $this->call($form, $form_state, 'form_submit');
    $_SESSION[$index][$hook][$step] = $values;
    $url = $this->canJump($form_state) ? $setting->url($form_state[$index]['jump']) : $setting->url($setting->nextStep($step));
    $errors = form_get_errors();

    if (FALSE == empty($errors)) {
      return;
    }

    if ($setting->lastStep() == $step) {
      unset($_SESSION[$index][$hook]);
      $url = $setting->exit;
    }

    drupal_goto($url);
  }

  protected function canJump(&$form_state) {
    $index = Core::INDEX;
    $jump = &$form_state[$index]['jump'];
    $setting = $this->setting;

    if ((FALSE == isset($jump)) || (FALSE == $setting->exist($jump))) {
      return FALSE;
    }

    $session = &$_SESSION[$index][$setting->hook];

    foreach ($setting->getSteps() as $step) {
      if ($step == $jump) {
        return TRUE;
      }

      if (FALSE == isset($session[$step])) {
        return FALSE;
      }
    }
    return FALSE;
  }

  protected function clearSession() {
    $setting = $this->setting;
    $clear = FALSE;

    if (FALSE == isset($_SESSION)) {
      return;
    }

    foreach ($setting->getSteps() as $step) {
      if ($clear) {
        unset($_SESSION[Core::INDEX][$setting->hook][$step]);
      }

      if ($step == $this->step) {
        $clear = TRUE;
      }
    }
  }
}
