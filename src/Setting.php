<?php

namespace Drupal\multi_step;

use Exception;

class Setting {

  var $hook = '';
  var $entry = 'node';
  var $exit = 'node';
  var $steps = [];

  function __construct($hook) {
    if (FALSE == is_string($hook)) {
      throw new Exception(t('Hook is not a string.'));
    }

    $call = $hook . '_multi_step_setting';

    if (FALSE == is_callable($call)) {
      throw new Exception(t('Hook finction %call is not exist.', ['%call' => $call]));
    }

    $setting = $call();

    if ((FALSE == is_array($setting)) || empty($setting['steps'])) {
      throw new Exception(t('Result of hook finction %call is empty or not a array.', ['%call' => $call]));
    }

    $this->merge($hook, $setting);
  }

  protected function merge($hook, array $setting) {
    $default = [
      'entry' => $this->entry($hook),
      'exit' => $this->exit,
      'steps' => [],
    ];

    $setting += $default;
    $steps = &$setting['steps'];
    $order = 0;

    foreach ($steps as $index => $item) {
      if (is_array($item)) {
        $order++;
        $steps[$index] = new Step($item, $order);
      }
    }

    foreach (array_keys($default) as $index) {
      $this->{$index} = $setting[$index];
    }

    $this->hook = $hook;
  }

  protected function entry($hook) {
    $entry = $this->entry;

    foreach (static::menu() as $index => $item) {
      if ($hook == $item['page arguments'][1]) {
        $entry = $index;
        break;
      }
    }

    return $entry;
  }

  static protected function menu() {
    static $cache = [];

    if (empty($cache)) {
      foreach (menu_get_router() as $index => $item) {
        if ((FALSE === strpos($index, '%')) && ('drupal_get_form' == $item['page callback']) && (1 < count($item['page arguments'])) && ('multi_step_form' == $item['page arguments'][0]) && TRUE) {
          $cache[$index] = $item;
        }
      }
    }

    return $cache;
  }

  /**
   * @return Setting
   */
  static function get($hook) {
    return Core::get('setting', $hook);
  }

  static function themeSteps(array $variables) {
    $items = &$variables['items'];
    $active = NULL;

    foreach ($items as $index => $item) {
      $item += [
        '#title' => t('Step @order', ['@order' => $index]),
        '#order' => $index,
      ];

      $items[$index]['data'] = '<span class="step-order">' . $item['#order'] . '</span><span class="step-title">' . $item['#title'] . '</span>';
      unset($items[$index]['#title'], $items[$index]['#order']);

      if ($index == $variables['active']) {
        $active = $index;
      }
    }

    if (isset($items[$active])) {
      $items[$active]['class'][] = 'active';
    }

    return theme('item_list', $variables);
  }

  /**
   * @return Step
   */
  function stepItem($step) {
    return $this->exist($step) ? $this->steps[$step] : NULL;
  }

  function exist($step) {
    return isset($this->steps[$step]);
  }

  function url($step) {
    return $this->exist($step) ? url($this->entry . '/' . $step, ['absolute' => TRUE]) : '';
  }

  function firstStep() {
    $steps = $this->getSteps();
    return reset($steps);
  }

  function getSteps() {
    return array_keys($this->steps);
  }

  function lastStep() {
    $steps = $this->getSteps();
    return end($steps);
  }

  function nextStep($step) {
    $steps = $this->getSteps();
    $index = array_search($step, $steps);
    return ((FALSE !== $index) && isset($steps[$index + 1])) ? $steps[$index + 1] : FALSE;
  }

  function formActions($step) {
    return [
      '#type' => 'actions',
      //      '#weight' => 100,
      'back' => $this->backAction($step),
      'next' => $this->nextAction($step),
    ];
  }

  function backAction($step) {
    $back = $this->backStep($step);

    if (FALSE == $back) {
      return [];
    }

    return [
      '#type' => 'link',
      '#title' => $this->steps[$step]->back,
      '#href' => $this->entry . '/' . $back,
      '#options' => ['absolute' => TRUE],
      '#attributes' => [
        'class' => [
          'ui',
          'button'
        ]
      ],
    ];
  }

  function nextAction($step) {
    return [
      '#type' => 'submit',
      '#value' => $this->steps[$step]->next,
      '#submit' => ['multi_step_form_submit'],
      '#attributes' => [
        'class' => [
          'ui',
          'button'
        ]
      ],
    ];
  }

  function backStep($step) {
    $steps = $this->getSteps();
    $index = array_search($step, $steps);
    return ((FALSE !== $index) && isset($steps[$index - 1])) ? $steps[$index - 1] : FALSE;
  }

  function formSteps($active = NULL) {
    return [
      '#markup' => theme('multi_step_steps', $this->prepareSteps($active)),
      '#attached' => ['css' => [drupal_get_path('module', 'multi_step') . '/multi_step.css']],
    ];
  }

  function prepareSteps($active = NULL) {
    $call = $this->hook . '_multi_step_steps';
    $steps = static::exportSteps($this->steps, $active);
    return is_callable($call) ? $call($steps) : $steps;
  }

  static function exportSteps(array $steps, $active = NULL) {
    $items = [];
    $attributes = [
      'class' => [
        'ui',
        'steps',
        'multi-step-steps'
      ]
    ];
    $order = 0;

    foreach ($steps as $index => $item) {
      $order++;
      $step = (array)$item;
      $step += [
        'title' => t('Step @order', ['@order' => $order]),
        'order' => $order,
      ];
      $items[$index] = [
        '#title' => $step['title'],
        '#order' => $step['order'],
        'class' => [
          'ui',
          'step'
        ],
      ];
    }

    return compact('items', 'attributes', 'active');
  }
}
