<?php
namespace Drupal\multi_step;

use stdClass;

class Step extends stdClass {

  var $title = '';
  var $back = '';
  var $next = '';
  var $multiforms = FALSE;
  var $order = '';

  function __construct(array $step, $order) {
    $default = [
      'title' => t('Step @order', ['@order' => $order]),
      'multiforms' => FALSE,
      'back' => t('Back'),
      'next' => t('Next'),
      'order' => $order,
    ];

    $step += $default;

    foreach (array_keys($default) as $index) {
      $this->{$index} = $step[$index];
    }
  }
}
